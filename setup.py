from setuptools import find_packages, setup

setup(
    name='giffer',
    version='0.1.0',    
    description='A tool to manually stabalize animations',
    url='https://gitlab.com/truprecht/giffer',
    author='Thomas Ruprecht',
    license='GNU General Public License v3.0',
    packages=find_packages(),
    install_requires=[
        'pillow',
        'numpy'],

    classifiers=[
        'Development Status :: 1 - Planning',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',  
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.8',
    ],
    entry_points={
        'gui_scripts': [
            'giffer=giffer.__main__:main',
        ],
    },
    )
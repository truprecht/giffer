# Giffer

Giffer is a small tool to manually stabilize animations.
In scenarios where subjects have moving parts or change in appearance, automatic image stabilization often fails or needs manual intervention.
This tool allows to define up to two reference points per frame in an animation, which are then used to apply geometric transformations (translation, scaling and rotation) such that the reference points are at the same position in each frame.
Ideally, these reference points are chosen to be fixed non-moving parts of the subject in the scenario.

![source animation](example/bee.gif) ![modified animation](example/bee_stable.gif)

([The animation](https://commons.wikimedia.org/wiki/File:Animation_090831_1656_mnvr.gif) was published under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en) by [Wopke wijngaard](https://commons.wikimedia.org/wiki/User:Wopke_wijngaard).)

# Usage

## Requirements & Installation

Giffer was developed with python 3.8.
`pip` is required to install, and the packages `numpy` and `pillow` are required to run this tool.
The required packages and Giffer can be automatically installed by changing into the project's directory and calling

    pip install .

If you want to process video files, `ffmpeg` is expected to be installed.

## Usage

After installing, run Giffer from a terminal using

    giffer examples/bee.gif

or any other path to a GIF, WebP or video file.
A window will pop open that shows the first frame of the animation and five buttons:
* `save`: opens a file dialog and saves all defined reference points,
* `load`: opens a file dialog and loads defined reference points,
* `export`: opens a file dialog, warps the frames to stabilize the animation and saves it,
* `previous` and `next`: cycle through the frames of the animation.

Reference points are defined by clicking on the frame, the left and right mouse buttons will create different reference points.
All defined reference points are shown in the frame using small red and blue rectangles.
The left and right arrow keys can be used to cycle through the frames of the animation.

![user interace](example/app.png)
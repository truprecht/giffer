
from pickle import load, dump
import tkinter as tk
import tkinter.filedialog as fd

from PIL import Image
from PIL.ImageTk import PhotoImage

from .gui.bounding_box_selection import ask_bounding_box
from .gui.preview_canvas import PreviewCanvas
from .gui.selector_canvas import SelectorCanvas
from .gui.wait import Wait
from .gui.zoom_canvas import ZoomCanvas
from .model.selections import Selections
from .model.imageloader import FileType, ImageLoader
from .model.warp import matrix, inv_matrix

class GifferApp(tk.Tk):
    def __init__(self, imagesfile, width, height, inputtype, loop, duration):
        super(GifferApp, self).__init__()
        self.loop = loop
        self.duration = duration
        self.width = width
        self.height = height

        load_images = lambda: ImageLoader(imagesfile, mode=(FileType(inputtype) if inputtype else None))
        self.images = Wait.block_while(self, "Loading frames ...", load_images)
        
        self.selections = Selections(len(self.images))
        self.selector_canvas = SelectorCanvas(self, width, height)
        self.selector_canvas.pack()
        self.create_preview_window()
        self.goto_img(0)
        self.selector_canvas.set_callback(self.set_coord)

        self.button_frame = tk.Frame(self)
        self.button_frame.pack(side=tk.BOTTOM, fill=tk.X)
        self.buttons = (
            tk.Button(self.button_frame, text="next", command=lambda: self.goto_img(self.current_image+1)),
            tk.Button(self.button_frame, text="previous", command=lambda: self.goto_img(self.current_image-1)),
            tk.Button(self.button_frame, text="save", command=self.save),
            tk.Button(self.button_frame, text="load", command=self.load),
            tk.Button(self.button_frame, text="export", command=self.export))
        for b in self.buttons[:2]:
            b.pack(side=tk.RIGHT)
        for b in self.buttons[2:]:
            b.pack(side=tk.LEFT)
        self.bind("d", self.toggle)
        self.bind("c", self.crop)
        self.bind("<Left>", lambda _: self.goto_img(self.current_image-1))
        self.bind("<Right>", lambda _: self.goto_img(self.current_image+1))

    def crop(self, event=None):
        fst_imgidx = next(i for i, s in enumerate(self.selections) if s.enabled)
        img = PhotoImage(self.images[fst_imgidx])
        chosen = ask_bounding_box(self, img, self.selections.crop or [-100]*4)
        if all(cc > 0 for cc in chosen):
            self.selections.crop = chosen
            self.show_preview()

    def toggle(self, event=None):
        self.selections[self.current_image].toggle()
        self.selector_canvas.toggle()

    def goto_img(self, i):
        if i < 0 or i >= len(self.images):
            return
        self.current_image = i
        self.selector_canvas.set_image(self.images[i], (self.selections[i, j] for j in range(0,2)))
        self.selector_canvas.enabled = self.selections[self.current_image].enabled
        self.title(f"Giffer ({i+1}/{len(self.images)})")
        self.show_preview()

    def set_coord(self, coordidx, xy):
        self.selections[self.current_image, coordidx] = xy
        self.show_preview()

    def show_preview(self):
        self.zoom_canvas.pack_forget()
        self.preview_canvas.pack(fill=tk.BOTH, expand=tk.YES)

        fst_sel = next(s for s in self.selections if s.enabled)
        ivm = inv_matrix(*fst_sel.coords)
        cimg = self.images[self.current_image]
        cc = self.selections[self.current_image].coords
        image1 = cimg.transform(cimg.size, Image.AFFINE, matrix(*cc, ivm))
        if not self.selections.crop is None:
            image1 = image1.crop(box=self.selections.crop)
        self.preview_canvas.set_image(image1)

    def show_zoom(self):
        self.preview_canvas.pack_forget()
        self.zoom_canvas.pack(fill=tk.BOTH, expand=tk.YES)

    def create_preview_window(self):
        self.preview_window = tk.Toplevel(self)
        self.preview_window.title("Preview")
        self.preview_window.protocol("WM_DELETE_WINDOW", self.close_preview_window)
        self.zoom_canvas = ZoomCanvas(self.preview_window, 5, 200, 200)
        self.preview_canvas = PreviewCanvas(self.preview_window, width=200, height=200)
        self.selector_canvas.bind_zoom_canvas(self.zoom_canvas)
        self.zoom_canvas.on_change(self.show_zoom)
        self.zoom_canvas.pack()

    def close_preview_window(self):
        self.selector_canvas.unbind_zoom_canvas()
        self.preview_window.destroy()

    def save(self):
        if not (sfn := fd.asksaveasfilename(filetypes=(("coordinate list","*.coli"), ("all files","*")), defaultextension=".coli")):
            return
        with open(sfn, "wb") as savefile:
            dump(self.selections, savefile)

    def load(self):
        if not (ofn := fd.askopenfilename(filetypes=(("coordinate list","*.coli"), ("all files","*")))):
            return
        with open(ofn, "rb") as openfile:
            self.selections = load(openfile)
        self.goto_img(0)

    def export(self):
        if not any(s.enabled for s in self.selections):
            return
        if not (sfn := fd.asksaveasfilename(filetypes=(("WebP","*.webp"), ("GIF","*.gif")), defaultextension=".webp")):
            return
        def warp_and_save():
            fst_sel = next(s for s in self.selections if s.enabled)
            ivm = inv_matrix(*fst_sel.coords)
            images1 = [
                img.transform(self.images[0].size, Image.AFFINE, matrix(*s.coords, ivm))
                for s, img in zip(self.selections, self.images) if s.enabled]
            if not self.selections.crop is None:
                images1 = [image.crop(box=self.selections.crop) for image in images1]
            if images1:
                images1[0].save(sfn, save_all=True, append_images=images1[1:], loop=self.loop, duration=self.duration, optimize=True)
        Wait.block_while(self, "Exporting animation ...", warp_and_save)

if __name__ == "__main__":
    mw = GifferApp([Image.open("example/app.png")], 500, 500, None, 0, 50)
    mw.mainloop()
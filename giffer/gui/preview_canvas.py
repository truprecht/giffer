import tkinter as tk
from PIL.ImageTk import PhotoImage

from .image_buffer import ImageBuffer

class PreviewCanvas(tk.Canvas):
    def __init__(self, parent, width=200, height=200):
        super(PreviewCanvas, self).__init__(parent, width=width, height=height)
        self.width, self.height = width, height
        self.image = None
        self.imageref = self.create_image(0, 0)
        self.bind("<Configure>", self._on_resize)

    def set_image(self, image):
        self.image = ImageBuffer(image)
        self._sync_size()
    
    def _on_resize(self, e):
        self.width, self.height = (e.width, e.height)
        self._sync_size()

    def _sync_size(self):
        imgratio = self.image._pilimage.width / self.image._pilimage.height
        cnvratio = self.width / self.height
        if cnvratio > imgratio:
            iheight, iwidth = self.height, int(self.image._pilimage.width*self.height/self.image._pilimage.height)
        else:
            iwidth, iheight = self.width, int(self.image._pilimage.height*self.width/self.image._pilimage.width)
        self.image.size = (iwidth, iheight)
        self.itemconfig(self.imageref, image=self.image.image)
        self.coords(self.imageref, int(self.width/2), int(self.height/2))
    
import tkinter as tk
from .image_buffer import ImageBuffer

colors = ["blue", "red", "yellow"]

class SelectorCanvas(tk.Canvas):
    def __init__(self, parent, width, height):
        super(SelectorCanvas, self).__init__(parent, width=width, height=height)
        self.height, self.width = height, width
        self.scalef = 1.0
        self.imgref = self.create_image(0, 0, anchor="nw")
        self.image = None
        self.pois = [None]*2
        self._currently_moving = None
        self.disabled = None
        self.callback = None 
        self.bind("<ButtonPress-1>", self.pick_up)
        self.bind("<B1-Motion>", self.drag)
        self.bind("<ButtonRelease-1>", self.release)
        self.bind("<Button-3>", self.add_poi)
        self.zoomcanvas = None

    def _nearest_image_pixel(self, x, y):
        return int((x+0.5)*self.scalef), int((y+0.5)*self.scalef)

    def _pixel_on_canvas(self, x, y):
        return int((x+0.5)/self.scalef), int((y+0.5)/self.scalef)

    def draw_poi(self, i, x, y):
        if self.image is None:
            return
        x, y = self._pixel_on_canvas(x, y)
        self.pois[i] = self.create_rectangle(x-5, y-5, x+5, y+5, outline=colors[i], activewidth=2)

    def add_poi(self, e):
        try:
            i = next(j for j, p in enumerate(self.pois) if p is None)
            x, y = self._nearest_image_pixel(e.x, e.y)
            self.draw_poi(i, x, y)
            if not self.callback is None:
                self.callback(i, (x, y))
        except StopIteration:
            pass

    def bind_zoom_canvas(self, canvas):
        self.zoomcanvas = canvas
        if not self.image is None:
            self.zoomcanvas.set_image(self.image._pilimage)

    def unbind_zoom_canvas(self):
        self.zoomcanvas = None

    def pick_up(self, e):
        # find poi that was clicked on
        for i, rectidx in enumerate(self.pois):
            if rectidx is None:
                continue
            left, upper, right, bottom = self.coords(rectidx)
            if e.x >= left and e.x <= right and e.y >= upper and e.y <= bottom:
                mx, my = e.x-left-5, e.y-upper-5
                self._currently_moving = (i, mx, my)
                if not self.zoomcanvas is None:
                    self.zoomcanvas.set_color(colors[i])
                    self.zoomcanvas.set_mid(*self._nearest_image_pixel(e.x-mx, e.y-my))
                return


    def drag(self, e):
        if self._currently_moving is None:
            return
        i, mx, my = self._currently_moving
        x, y = self._nearest_image_pixel(e.x-mx, e.y-my)
        cx, cy = self._pixel_on_canvas(x, y)
        self.coords(self.pois[i], cx-5, cy-5, cx+5, cy+5)
        if not self.zoomcanvas is None:
            self.zoomcanvas.set_mid(x, y)
    
    def release(self, e):
        if self._currently_moving is None:
            return
        if not self.callback is None:
            i, mx, my = self._currently_moving
            x, y = self._nearest_image_pixel(e.x-mx, e.y-my)
            self.callback(i, (x, y))
        self._currently_moving = None

    def set_image(self, image, pois=[], enabled=True):
        if not self.zoomcanvas is None:
            self.zoomcanvas.set_image(image)

        self.scalef = max(image.height / self.height, image.width / self.width)
        width, height = int(image.width / self.scalef), int(image.height / self.scalef)
        self.configure(width=width, height=height)
        self.image = ImageBuffer(image, size=(width, height))
        if self.enabled != enabled:
            self.toggle()
        elif self.enabled:
            self.itemconfig(self.imgref, image=self.image.image)
        else:
            self.itemconfig(self.imgref, image=self.image.gray_image)
        for p in self.pois:
            if not p is None:
                self.delete(p)
        self.pois = [None]*2
        for i, xy in enumerate(pois):
            if not xy is None:
                x, y = xy
                self.draw_poi(i, x, y)

    @property
    def enabled(self):
        return self.disabled is None

    @enabled.setter
    def enabled(self, v):
        if v and not self.enabled:
            self.itemconfig(self.imgref, image=self.image.image)
            self.delete(self.disabled)
            self.disabled = None
        if not v and self.enabled:
            self.itemconfig(self.imgref, image=self.image.gray_image)
            self.disabled = self.create_text(30, 15, fill="red", text="disabled")

    def toggle(self):
        self.enabled = not self.enabled

    def set_callback(self, c):
        self.callback = c
        

if __name__ == "__main__":
    from PIL import Image
    def cb(i, xy):
        print(i, xy)
    root = tk.Tk()
    sc = SelectorCanvas(root, 500, 500)
    sc.set_callback(cb)
    sc.set_image(Image.open("example/app.png"), [(10,10)])
    sc.pack()
    def t(_):
        sc.toggle()
    root.bind("d", t)
    root.mainloop()
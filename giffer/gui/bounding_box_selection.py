import tkinter as tk

def ask_bounding_box(parent: tk.Widget, img, initial_values = [-100]*4):
    coords = list(initial_values)
    selector = BoundingBoxSelection(parent, img, coords)
    selector.wait_visibility()
    selector.transient(parent)
    selector.update_idletasks()
    selector.focus_force()
    selector.grab_set()
    selector.wait_window()

    x1, y1, x2, y2 = coords
    left = min(x1, x2)
    right = max(x1, x2)
    upper = min(y1, y2)
    bottom = max(y1, y2)
    return left, upper, right, bottom


class BoundingBoxSelection(tk.Toplevel):
    def __init__(self, parent: tk.Widget, img, variables):
        tk.Toplevel.__init__(self, parent)
        self.title("Select bounds")
        self.coords = variables
        w, h = img.width(), img.height()
        self.canvas = tk.Canvas(self, width=w, height=h)
        self.canvas.pack(side=tk.TOP)
        self.imref = self.canvas.create_image(0, 0, anchor="nw", image=img)
        self.rectref = None
        if any(iv < 0 for iv in variables):
            self.coords[:] = list(map(int, (w*0.2, h*0.2, w*0.8, h*0.8)))
        self.draw_coords()
        self.canvas.bind("<ButtonPress-1>", self.gather_rect_callback)
        self.canvas.bind("<B1-Motion>", self.move_rect_callback)
        self.canvas.bind("<ButtonRelease-1>", self.drop_rect_callback)

        self.button_frame = tk.Frame(self)
        self.button_frame.pack(side=tk.BOTTOM, fill=tk.X)
        self.okbut = tk.Button(self.button_frame, text="Ok", command=self.destroy)
        self.okbut.pack(side=tk.RIGHT)
        self.cancelbut = tk.Button(self.button_frame, text="Cancel", command=self.cancel)
        self.cancelbut.pack(side=tk.RIGHT)
        self.protocol("WM_DELETE_WINDOW", self.cancel)

    def cancel(self):
        self.coords[:] = [-100]*4
        self.destroy()

    def draw_coords(self):
        if self.rectref:
            self.canvas.delete(self.rectref)
            self.canvas.delete(self.dragreflu)
            self.canvas.delete(self.dragrefrb)
        self.rectref = self.canvas.create_rectangle(*self.coords, outline="red")
        self.dragreflu = self.canvas.create_rectangle(*self.coords[:2], *map(lambda x: x+5, self.coords[:2]), outline="red", activewidth=2)
        self.dragrefrb = self.canvas.create_rectangle(*self.coords[2:], *map(lambda x: x-5, self.coords[2:]), outline="red", activewidth=2)

    def gather_rect_callback(self, event):
        if event.x >= self.coords[0] and event.x <= self.coords[0]+5 and \
                event.y >= self.coords[1] and event.y <= self.coords[1]+5:
            self.moving = slice(0,2)
        elif event.x >= self.coords[2]-5 and event.x <= self.coords[2] and \
                event.y >= self.coords[3]-5 and event.y <= self.coords[3]:
            self.moving = slice(2,4)
        else:
            self.moving = None
    
    def move_rect_callback(self, event):
        if self.moving is None:
            return
        self.coords[self.moving] = event.x, event.y
        self.draw_coords()

    def drop_rect_callback(self, event):
        self.moving = None

    def get_coords(self):
        x1, y1, x2, y2 = self.coords
        left = min(x1, x2)
        right = max(x1, x2)
        upper = min(y1, y2)
        bottom = max(y1, y2)
        return left, upper, right, bottom

if __name__ == "__main__":
    root = tk.Tk()
    img = tk.PhotoImage(file="example/app.png")
    def cb():
        print(ask_bounding_box(root, img))
    b = tk.Button(root, text="do!", command=cb)
    b.pack()
    root.mainloop()
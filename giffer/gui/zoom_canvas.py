import tkinter as tk
from PIL.ImageTk import PhotoImage

from .image_buffer import ImageBuffer

class ZoomCanvas(tk.Canvas):
    def __init__(self, parent, factor: int = 10, width=200, height=200):
        self.width, self.height = width, height
        super(ZoomCanvas, self).__init__(parent, width=width, height=height)
        self.zoom_factor = factor
        self.change_callback = None
        self.image = None
        self.imageref = self.create_image(0, 0, anchor="nw")

        self.left = int(int(self.width/self.zoom_factor)/2)
        self.upper = int(int(self.height/self.zoom_factor)/2)
        right = self.left+1
        bottom = self.upper+1
        self.rectref = self.create_rectangle(*(x*self.zoom_factor for x in (self.left, self.upper, right, bottom)))

        self.bind("<Configure>", self._on_resize)

    def set_image(self, image):
        self.image = ImageBuffer(image, size=(image.width*self.zoom_factor, image.height*self.zoom_factor))
        self.itemconfig(self.imageref, image=self.image.image)

    def set_mid(self, x, y):
        x, y = int(-(x-self.left)*self.zoom_factor), int(-(y-self.upper)*self.zoom_factor)
        self.coords(self.imageref, x, y)
        if not self.change_callback is None:
            self.change_callback()

    def set_color(self, colorname):
        self.itemconfig(self.rectref, outline=colorname)

    def on_change(self, callback):
        self.change_callback = callback
        
    def _on_resize(self, e):
        self.width=e.width
        self.height=e.height

        self.left = int(int(self.width/self.zoom_factor)/2)
        self.upper = int(int(self.height/self.zoom_factor)/2)
        right = self.left+1
        bottom = self.upper+1
        self.coords(self.rectref, *(x*self.zoom_factor for x in (self.left, self.upper, right, bottom)))

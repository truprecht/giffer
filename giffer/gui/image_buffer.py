from PIL.ImageTk import PhotoImage
from PIL.ImageOps import grayscale


class ImageBuffer:
    def __init__(self, pilimage, size=None):
        self._pilimage = pilimage
        self._size = (pilimage.width, pilimage.height) if size is None else size
        self._tkimage = None
        self._grayimage = None

    @property
    def size(self):
        return self._size

    @size.setter
    def size(self, s: (int, int)):
        self._size = s
        self._tkimage = None
        self._grayimage = None

    @property
    def image(self):
        if self._tkimage is None:
            self._tkimage = PhotoImage(self._pilimage.resize(self._size))
        return self._tkimage

    @property
    def gray_image(self):
        if self._grayimage is None:
            self._grayimage = PhotoImage(grayscale(self._pilimage.resize(self._size)))
        return self._grayimage
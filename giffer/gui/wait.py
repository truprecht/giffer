import tkinter as tk

class Wait:
    class BlockingModal(tk.Toplevel):
        def __init__(self, parent: tk.Widget, message: str):
            tk.Toplevel.__init__(self, parent)
            self.title("Please wait ...")
            self.label = tk.ttk.Label(self, text=f"Please wait for the pending operation:\n{message}")
            self.label.pack(padx=20, pady=10)
            self.wait_visibility()
            self.label.wait_visibility()
            self.protocol("WM_DELETE_WINDOW", lambda: None)
            self.transient(parent)

        def block_operation(self, operation):
            self.update_idletasks()
            self.focus_force()
            self.grab_set()
            value = operation()
            self.grab_release()
            return value

        def block(self):
            self.update_idletasks()
            self.focus_force()
            self.grab_set()

        def release(self):
            self.grab_release()

    @classmethod
    def block_while(cls, parent: tk.Widget, message: str, operation):
        window = cls.BlockingModal(parent, message)
        window.block()
        retval = operation()
        window.release()
        window.destroy()
        return retval
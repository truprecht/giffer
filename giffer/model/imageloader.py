from PIL import Image, ImageSequence
from itertools import chain
from contextlib import closing
from subprocess import run
from tempfile import TemporaryDirectory, TemporaryFile
from os import listdir
from enum import Enum


class FileType(Enum):
    IMAGECONTAINER = "GIF"
    FILELIST = "LIST"
    VIDEO = "VIDEO"

    @classmethod
    def from_extension(cls, extension: str):
        __EXTENSIONS__ = {
            cls.IMAGECONTAINER: ("gif", "webp"),
            cls.FILELIST: ("txt",),
            cls.VIDEO: ("mov", "mp4", "mkv") }

        __EXTENSION_TO_TYPE__ = {
            ex: t
            for (t, exs) in __EXTENSIONS__.items()
            for ex in exs }

        return __EXTENSION_TO_TYPE__[extension]

class ImageLoader:
    def __init__(self, filename: str, mode: FileType = None):
        if mode is None:
            extension = filename.split(".")[-1].lower()
            mode = FileType.from_extension(extension)

        if mode == FileType.IMAGECONTAINER:
            imagecontainer = Image.open(filename)
            self.cached_images = [f.copy() for f in ImageSequence.Iterator(imagecontainer)]
            self.uncached_images = []
        if mode == FileType.FILELIST:
            with open(filename) as filenames:
                self.uncached_images = [
                    line.strip() for line in filenames]
            self.cached_images = []
        if mode == FileType.VIDEO:
            assert run(("ffmpeg", "-loglevel", "quiet")), "video file cannot be opened because ffmpeg is not installed"
            with TemporaryDirectory() as tmpdir:
                run(["ffmpeg", "-r", "1", "-i", filename, "-r", "1", f"{tmpdir}/%03d.png", "-loglevel", "quiet"])
                self.cached_images = [ Image.open(f"{tmpdir}/{fn}")
                    for fn in sorted(listdir(tmpdir)) if fn.endswith("png") ]
            self.uncached_images = []

    def __getitem__(self, index: int) -> Image:
        if type(index) is slice:
            start, stop, step = index.indices(len(self))
            return [self[i] for i in range(start, stop, step)]

        if index >= len(self):
            raise IndexError()
        
        for _ in range(index-len(self.cached_images)):
            filename = self.uncached_images.pop(0)
            self.cached_images.append(Image.open(filename))

        return self.cached_images[index]

    def __len__(self) -> int:
        return len(self.cached_images) + len(self.uncached_images)
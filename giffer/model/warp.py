from math import sqrt, atan, degrees, cos, sin, pi
import numpy as np

from PIL import Image

from .selections import Selection

def __matrices__(xy1, xy2):
    if xy1 is None:
        # use snd coordinate if first was not selected
        xy1 = xy2
        xy2 = None

    if xy1 is None:
        # no coordinate was selected
        t = np.zeros((2, 3))
    else:
        x1, y1 = xy1
        t = np.array([[0, 0, x1],
                      [0, 0, y1]])
    
    if xy2 is None:
        # if there's only one coordinate, skip rotation and scaling
        R = np.identity(2)
        S = np.identity(2)
    else:
        x2, y2 = xy2
        dx, dy = x1-x2, y1-y2
        s = sqrt(dx**2 + dy**2)
        if dx == 0 and dy > 0:
            theta = pi/2
        elif dx == 0 and dy < 0:
            theta = -pi/2
        else:
            theta = atan(dy/dx) + (0 if dx > 0 else pi)
        sint = sin(theta)
        cost = cos(theta)
        
        R = np.array([[cost, -sint],
                     [sint,  cost]])
        S = np.identity(2) * s

    return t, R, S

def inv_matrix(xy1, xy2):
    t, R, S = __matrices__(xy1, xy2)
    t = np.array([[1, 0, 0],
                  [0, 1, 0]]) - t
    S = np.divide(1, S, where=S!=0)
    return S @ R.transpose() @ t

def matrix(xy1, xy2, ivm):
    t, R, S = __matrices__(xy1, xy2)
    m = t + (R @ S @ ivm)
    return tuple(m[0]) + tuple(m[1])
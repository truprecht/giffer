class Selection:
    def __init__(self):
        self.__coords__ = [None]*4
        self.__enabled__ = True

    def toggle(self):
        self.__enabled__ = not self.__enabled__

    @property
    def enabled(self):
        return self.__enabled__

    @property
    def coords(self):
        return self[0], self[1]

    def __getitem__(self, idx):
        assert idx < 2
        cc = self.__coords__[2*idx:2*(idx+1)]
        if self.enabled and all(not c is None for c in cc):
            return tuple(cc)

    def __setitem__(self, idx, val):
        assert idx < 2 and len(val) == 2
        self.__enabled__ = True
        self.__coords__[2*idx:2*(idx+1)] = val

    def __str__(self):
        act = "enabled" if self.enabled else "disabled"
        return f"{self.coords} ({act})"

class Selections:
    def __init__(self, count):
        self.__selections__ = [Selection() for _ in range(count)]
        self.crop = None

    def _nearest_set_index(self, imageidx, coordidx):
        for distance in range(1, len(self)):
            for idx in (imageidx-distance, imageidx+distance):
                if idx >= 0 and idx < len(self) and not self[idx][coordidx] is None:
                    return idx
    
    def __len__(self):
        return len(self.__selections__)

    def __setitem__(self, index, value):
        assert len(index) == 2 and len(value) == 2 and all(v >= 0 for v in value)
        imageidx, coordidx = index
        assert imageidx < len(self) and coordidx < 2
        self.__selections__[imageidx][coordidx] = value

    def __getitem__(self, index):
        if type(index) == tuple and len(index) == 2:
            imgidx, coordidx = index
            if self[imgidx].enabled and self[imgidx][coordidx] == None and \
                    not (n := self._nearest_set_index(imgidx, coordidx)) is None:
                self[imgidx][coordidx] = self[n][coordidx]
            return self[imgidx][coordidx]
        if index < 0 or index >= len(self):
            raise IndexError()
        return self.__selections__[index]

    def __str__(self):
        return "\n".join(str(s) for s in self.__selections__) + f"\ncrop: {self.crop}"

def test():
    wi = Selections(10)
    assert wi[0, 0] is None
    wi[3, 0] = (1,1)
    wi[7, 0] = (2,2)
    wi[5, 1] = (3,3)
    assert wi[0, 0] == (1,1)
    assert wi[6, 0] == (2,2)
    assert wi[5, 0] == (2,2)
    assert wi[4, 0] == (1,1)
    assert wi[1, 1] == (3,3)

if __name__ == "__main__":
    test()
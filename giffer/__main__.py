from argparse import ArgumentParser
from collections import defaultdict
from itertools import chain, repeat
import pickle

import tkinter as tk
import tkinter.filedialog as fd
from tkinter import ttk
from PIL import Image
from PIL.ImageTk import PhotoImage
from PIL.ImageOps import grayscale

from .model.imageloader import ImageLoader, FileType
from .model.warp import matrix, inv_matrix
from .model.selections import Selections
from .gui.wait import Wait
from .gui.bounding_box_selection import ask_bounding_box
from .gui.selector_canvas import SelectorCanvas
from .gui.zoom_canvas import ZoomCanvas
from .giffer_app import GifferApp

def main():
    args = ArgumentParser()
    args.add_argument("images")
    args.add_argument("--maxwidth", type=int, default=768)
    args.add_argument("--maxheight", type=int, default=512)
    args.add_argument("--inputtype", choices=[t.value for t in FileType])
    args.add_argument("--loop", type=int, default=0)
    args.add_argument("--duration", type=int, default=40)
    args = args.parse_args()


    giffer = GifferApp(args.images, args.maxwidth, args.maxheight, args.inputtype, args.loop, args.duration)
    giffer.mainloop()

if __name__ == "__main__":
    main()